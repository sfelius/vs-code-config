# README #

This README explains how to set up Visual Studio for developing for Salesforce Commerce Cloud

### How to setup VS Code for SFCC Development ###

* Install [Prophet Debugger Extension](https://marketplace.visualstudio.com/items?itemName=SqrTT.prophet) from Visual Studio Marketplace.
* Configure `launch.json` file according to instructions from the extension.
* Unpack files from this repo into folder that contains your cartridges. 
* To get the latest version of [dw-api-types](https://bitbucket.org/demandware/dw-api-types) and unpack in `dw` folder.
* Configure your `jsconfig.json` file to make sure paths are correct. The example below assumes your type cartridges are in the root and your type definitions folder `dw` is as well.


```JSON
{
    "compilerOptions": {
        "noLib": true,
        "target": "es5",
        "baseUrl": "./",
        "paths": {
            "*" : ["./*", "modules/*", "./dw/*"]
        }
    },
    "typeAcquisition": {
        "enable": true
    },
    "include": [
        "./dw/*.d.ts",
        "./dw/**/*.d.ts",
        "./*/cartridge/scripts/**/*.js",
        "./*/cartridge/controllers/**/*.js",
        "./modules/**/*.js"
    ]
}
```

* (OPTIONAL) - Install [ESLint Extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) from Visual Studio Marketplace
