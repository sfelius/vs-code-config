import PaymentInstrument = require('../order/PaymentInstrument');


declare class CustomerPaymentInstrument extends PaymentInstrument {
    bankAccountDriversLicense  :  string;
    bankAccountnumber  :  string;
    creditCardnumber  :  string;
    getBankAccountDriversLicense() : string;
    getBankAccountnumber() : string;
    getCreditCardnumber() : string;
}

export = CustomerPaymentInstrument;