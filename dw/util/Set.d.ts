import Collection = require('./Collection');

declare class Set<T> extends Collection<T> {
    public EMPTY_SET  :  Set<T>;
}

export = Set;