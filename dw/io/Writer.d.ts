import OutputStream = require('./OutputStream')

declare class Writer {
    /**
     * Create a writer from a stream using UTF-8 character encoding.
     * @param stream 
     */
    constructor(stream : OutputStream)

    /**
     * Create a writer from a stream using the specified character encoding.
     * @param stream 
     * @param encoding 
     */
    constructor(stream : OutputStream, encoding : string)

    /**
     * Closes the writer.
     */
    close() : void

    /**
     * Flushes the buffer.
     */
    flush() : void

    /**
     * Write the given string to the stream.
     * @param str 
     */
    write(str : string) : void

    /**
     * Write the given string to the stream.
     * @param str 
     * @param off 
     * @param len 
     */
    write(str : string, off : number, len : number) : void
}

export = Writer