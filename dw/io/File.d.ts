import List = require('../util/List')

declare class File {
    static CATALOGS  :  string;
    static DYNAMIC  :  string;
    static IMPEX  :  string;
    static LIBRARIES  :  string;
    static SEPARATOR  :  string;
    static STATIC  :  string;
    static TEMP  :  string;

    directory  :  Boolean;
    file  :  Boolean;
    fullPath  :  string;
    name  :  string;
    rootDirectoryType  :  string;

    /**
     * Creates a File from the given absolute file path in the file namespace.
     * @param absPath 
     */
    constructor(absPath : string);

    /**
     * Creates a File given a root directory and a relative path.
     * @param rootDir 
     * @param relPath 
     */
    constructor(rootDir : File, relPath : string);

    /**
     * Copy a file.
     * @param file 
     */
    copyTo(file : File) : File

    /**
     * Create file.
     */
    createNewFile() : boolean;

    /**
     * Indicates if the file exists.
     */
    exists() : boolean;

    /**
     * Return the full file path denoted by this File.
     */
    getFullPath() : string;

    /**
     * Returns the name of the file or directory denoted by this object.
     */
    getName() : string;

    /**
     * Returns a File representing a directory for the specified root directory type.
     * @param rootDir 
     * @param args 
     */
    static getRootDirectory(rootDir : string, ...args : string[]) : File

    /**
     * Returns the root directory type, e.g.
     */
    getRootDirectoryType() : string;

    /**
     * Assumes this instance is a gzip file.
     * @param root 
     */
    gunzip(root : File) : void;

    /**
     * GZip this instance into a new gzip file.
     * @param outputZipFile 
     */
    gzip(outputZipFile : File) : void;

    /**
     * Indicates that this file is a directory.
     */
    isDirectory() : boolean;

    /**
     * Indicates if this file is a file.
     */
    isFile() : boolean;

    /**
     * Return the time, in milliseconds, that this file was last modified.
     */
    lastModified() : number;

    /**
     * Return the length of the file in bytes.
     */
    length() : number;

    /**
     * Returns an array of strings naming the files and directories in the directory denoted by this object.
     */
    list() : string[];

    /**
     * Returns an array of File objects in the directory denoted by this File.
     */
    listFiles() : List<File>;

    /**
     * Returns an array of File objects denoting the files and directories in the directory denoted by this object that satisfy the specified filter.
     * @param filter 
     */
    listFiles(filter : (file : File) => boolean) : List<File>;

    /**
     * Returns an MD5 hash of the content of the file of this instance.
     */
    md5() : string;

    /**
     * Creates a directory.
     */
    mkdir() : boolean;

    /**
     * Creates a directory, including, its parent directories, as needed.
     */
    mkdirs() : boolean;

    /**
     * Deletes the file or directory denoted by this object.
     */
    remove() : boolean;

    /**
     * Rename file.
     * @param file 
     */
    renameTo(file : File) : boolean;

    /**
     * Assumes this instance is a zip file.
     * @param root 
     */
    unzip(root : File) : void;

    /**
     * Zip this instance into a new zip file.
     * @param outputZipFile 
     */
    zip(outputZipFile : File) : void;

}

export = File; 