import ProductInventoryRecord = require('./ProductInventoryRecord');
import ProductAvailabilityLevels = require('./ProductAvailabilityLevels');


declare class ProductAvailabilityModel {
    AVAILABILITY_STATUS_BACKORDER  :  string;
    AVAILABILITY_STATUS_IN_STOCK  :  string;
    AVAILABILITY_STATUS_NOT_AVAILABLE  :  string;
    AVAILABILITY_STATUS_PREORDER  :  string;
    availability  :  number;
    availabilityStatus  :  string;
    inStock  :  boolean;
    inventoryRecord  :  ProductInventoryRecord;
    orderable  :  Boolean;
    SKUCoverage  :  number;
    timeToOutOfStock  :  number;

    getAvailability() : number;
    getAvailabilityLevels(quantity : number) : ProductAvailabilityLevels;
    getAvailabilityStatus() : string;
    getInventoryRecord() : ProductInventoryRecord;
    getSKUCoverage() : number;
    getTimeToOutOfStock() : number;
    isInStock(quantity : number) : boolean;
    sInStock() : Boolean;
    isOrderable(quantity : number) : boolean;
    isOrderable() : boolean;

}

export = ProductAvailabilityModel;